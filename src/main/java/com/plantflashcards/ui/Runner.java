package com.plantflashcards.ui;

import java.util.ArrayList;
import java.util.List;

import com.plantflashcards.dto.IPlant;
import com.plantflashcards.dto.Plant;
import com.plantflashcards.dto.Tree;

public class Runner {

	public static void main(String[] args) {
		list();
	}

	private static void list() {

		IPlant plant  = new Plant();
		plant.setGenus("Cercis");
		plant.setSpecies("Canadas");
		plant.setCultivar("Alba");
		
		
		IPlant tree = new Tree();
		tree.setGenus("Ainima");
		tree.setSpecies("Tribola");
		tree.setCultivar("Potomac");
		tree.setHeight(4);
		
		
		List <IPlant> allPlants = new ArrayList<IPlant>();
		allPlants.add(plant);
		allPlants.add(tree);
		for (IPlant iPlant : allPlants) {
			System.out.println("Plant :" + iPlant.getScientificName());
		}
		
	}

}
