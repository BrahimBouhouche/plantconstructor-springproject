package com.plantflashcards.dto;

public class Plant implements IPlant {

	private String genus;
	private String species;
	private String cultivar;
	private int height;

	
	
	public String getScientificName() {
		return "plant " + getGenus() + getSpecies() + getCultivar() + getCultivar() ;
	}

	/**
	 * @return the cultivar
	 */
	public String getCultivar() {
		return cultivar;
	}

	/**
	 * @param cultivar the cultivar to set
	 */
	public void setCultivar(String cultivar) {
		this.cultivar = cultivar;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the genus
	 */
	public String getGenus() {
		return genus;
	}

	/**
	 * @param genus
	 *            the genus to set
	 */
	public void setGenus(String genus) {
		this.genus = genus;
	}

	/**
	 * @return the species
	 */
	public String getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *            the species to set
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

	
	
}
