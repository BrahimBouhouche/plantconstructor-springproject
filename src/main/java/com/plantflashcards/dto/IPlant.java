package com.plantflashcards.dto;

/**
 * 
 * @author Brahim
 *
 */
public interface IPlant {
	

	public String getScientificName();

	void setSpecies(String species);

	void setGenus(String genus);

	void setHeight(int height);

	void setCultivar(String cultivar);

}
