package com.plantflashcards.dto;

public class Tree extends Plant {
	
	private String fallColor;
	
	
	
	
	@Override
	public String getScientificName() {
		return " tree " + super.getScientificName();
	}



	/**
	 * @return the fallColor
	 */
	public String getFallColor() {
		return fallColor;
	}



	/**
	 * @param fallColor the fallColor to set
	 */
	public void setFallColor(String fallColor) {
		this.fallColor = fallColor;
	}



	
}
